import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../models/user.dart';

class Users with ChangeNotifier {
  List<Result> users = [];
  List<Result> get getUsers => [...users];
  
  Future getData() async {
    String url = 'https://randomuser.me/api';
    try {
      http.Response response = await http.get(url); 
      User model = User.fromJson(json.decode(response.body));
      List<Result> loadedUser = model.results;
      users = loadedUser;
    } catch(error) {
      print(error);
    }
  }
}