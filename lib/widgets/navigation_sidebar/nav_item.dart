import 'package:flutter/material.dart';
import '../../locator.dart';
import '../../services/navigation_service.dart';
import '../../extensions/hover_extensions.dart';

class NavBarItem extends StatelessWidget {
  final IconData icon;
  final String title;
  final String navigationPath;
  const NavBarItem(this.icon, this.title, this.navigationPath);

  @override
  Widget build(BuildContext context) {
    return GestureDetector( 
      onTap: () {
        // DON'T EVER USE A SERVICE DIRECTLY IN THE UI TO CHANGE ANY KIND OF STATE
        // SERVICES SHOULD ONLY BE USED FROM A VIEWMODEL
        locator<NavigationService>().navigateTo(navigationPath);
      },
      child: Wrap(
        crossAxisAlignment: WrapCrossAlignment.center,
        children: [
          Icon(
            icon,
            color: Colors.white,
          ),
          SizedBox(width: 8.0),
          Text(
            title,
            style: TextStyle(
              color: Colors.white,
              fontSize: 18.0
            ),
          ).showCursorOnHover,
        ],
      ),
    );
  }
}
