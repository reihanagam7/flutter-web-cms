import 'package:flutter/material.dart';
import 'package:project/routing/route_names.dart';

import './nav_item.dart';

class NavigationSideBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 300.0,
      height: double.infinity,
      color: Colors.indigo[600],
      child: Container(
        child: Column(
          children: [
            Container(
              width: double.infinity,
              height: 250.0,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  NavBarItem(Icons.home, 'Home', HomeRoute),
                  NavBarItem(Icons.account_circle, 'Profile', ProfileRoute),
                  NavBarItem(Icons.text_format,'About', AboutRoute),
                ]
              )
            )
          ],
        )
      )
    );
  }
}