import 'package:flutter/material.dart';
import 'package:project/providers/users.dart';
import 'package:provider/provider.dart';
import 'package:responsive_builder/responsive_builder.dart';

class ProfileView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Card(
        elevation: 10.0,
        margin: EdgeInsets.all(80.0),
        color: Colors.white,
        child: Consumer<Users>(
          builder: (context, user, child) => FutureBuilder(
            future: user.getData(),
            builder: (context, snapshot) {
              if(snapshot.connectionState == ConnectionState.waiting) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
              if(snapshot.hasError) {
                return Center(
                  child: Text('Oops! Something went wrong! Please Try Again.'),
                );
              }
              return ListView.builder(
                itemCount: user.users.length,
                itemBuilder: (context, index) => Row(
                  children: [
                    Container(
                      margin: EdgeInsets.only(left: 40.0),
                      child: ClipOval(
                        child: Image.network('${user.users[index].picture.large}')
                      )
                    ),
                    Container(
                      margin: EdgeInsets.all(50.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Wrap(
                            children: [
                              Text('NAME : ', style: TextStyle(
                                fontWeight: FontWeight.bold
                              )),
                              Text('${user.users[index].name.last}')
                            ],
                          ),
                          SizedBox(height: 25.0),
                          Wrap(
                            children: [
                              Text('AGE : ', style: TextStyle(
                                fontWeight: FontWeight.bold
                              )),
                              Text('${user.users[index].dob.age}')
                            ],
                          ),
                          SizedBox(height: 25.0),
                          Wrap(
                            children: [
                              Text('GENDER : ', style: TextStyle(
                                fontWeight: FontWeight.bold
                              )),
                              Text('${user.users[index].gender}')
                            ],
                          ),
                        ]
                      ),
                    )
                  ],
                ),
              );
            }
          ),
        )
      )
    );
  }
}